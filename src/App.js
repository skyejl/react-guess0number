import React from 'react';
import './App.less';
import GameBoard from "./GameBoard.js";

class App extends React.Component{

    constructor(props, context) {
        super(props, context);
        this.state = {
            create: false,
            gameId: "",
            hint: "",
            correct: false
        }
        this.handleCreateGame = this.handleCreateGame.bind(this);
        this.submitNumbers = this.submitNumbers.bind(this);
    }

    handleCreateGame() {
        this.setState({
            create: true
        });
        fetch("http://localhost:8080/api/games",{
            method: "POST",
            mode: "cors",

        }).then(res => {
            const resId = res.headers.get("Location");
            this.setState({
                gameId: resId
            });
            console.log(this.state.gameId)
        })
    }
    submitNumbers(val) {
        let gamePath = this.state.gameId;
        // console.log(val);
        // console.log(val.join(""));
        fetch("http://localhost:8080"+gamePath, {
            method: "PATCH",
            headers: {
                "Content-Type": "application/json;charset=UTF-8"
            },
            body: JSON.stringify({ "answer": val.join("") }),
        }).then(res => res.json()).then(res => {
            let hint = res.hint;
            let correct = res.correct;
            this.setState({
                hint: hint,
                correct: correct
            })
        })
    }

    render() {
        const isCreate = this.state.create;
        return (
        <div className="App">
            <button className="gameButton" onClick={this.handleCreateGame}>NEW GAME</button>
            {isCreate ? (
                <GameBoard submitNumbers={this.submitNumbers} hint={this.state.hint} correct={this.state.correct}/>
            ) : (
                <div></div>
            )}
        </div>
        );
    }
}
export default App;

